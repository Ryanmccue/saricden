import { SaricdenPage } from './app.po';

describe('saricden App', () => {
  let page: SaricdenPage;

  beforeEach(() => {
    page = new SaricdenPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
